import { Meteor } from "meteor/meteor"
import { _ } from 'underscore'

if(Meteor.isServer){
	Meteor.methods({
		downloadExcel: function(params, funcMap) {
			check(params, Object)
			check(funcMap, Match.Maybe(Function))
			console.log(funcMap)
			let col = params.col || '';
			let selector = params.selector || {};
			let joins = params.joins || {};
			let projection = params.projection || {};
			let agg = params.aggregate || [];
			let aggregate = [];
			//console.log(projection);

			if(joins){
				_.each(joins,function(v,k){
					aggregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
					if(v.unwind){
						aggregate.push({$unwind:{path:'$'+v.as,preserveNullAndEmptyArrays:true}});
					}
				});
			}
			if(!_.isEmpty(aggregate)||!_.isEmpty(agg)){
				aggregate.push({$match:selector});
				aggregate=aggregate.concat(agg);
				var collection = Meteor.Collection.get(col).aggregate(aggregate);
				//ReactiveAggregate(this, col, agregate)
				//console.log(collection);
			}else{
				var collection = Meteor.Collection.get(col).find(selector,projection).fetch();
			}
			if(funcMap){
				collection = collection.map(funcMap)
			}
			//var collection = Collections[col].find(selector,projection).fetch();
			var heading = true; // Optional, defaults to true
			var delimiter = "," // Optional, defaults to ",";
			return exportcsv.exportToCSV(collection, heading, delimiter);
		},
		downloadPlainExcel: function(params, funcMap) {
			check(params, Object)
			check(funcMap, Match.Maybe(Function))
			let col = params.col || '';
			let selector = params.selector || {};
			let joins = params.joins || {};
			let projection = params.projection || {};
			let agg = params.aggregate || [];
			let aggregate = [];
			//console.log(projection);

			if(joins){
				_.each(joins,function(v,k){
					aggregate.push({$lookup:_.pick(v,'from','localField','foreignField','as')});
					if(v.unwind){
						aggregate.push({$unwind:{path:'$'+v.as,preserveNullAndEmptyArrays:true}});
					}
				});
			}
			if(!_.isEmpty(aggregate)||!_.isEmpty(agg)){
				aggregate.push({$match:selector});
				aggregate=aggregate.concat(agg);
				var collection = Meteor.Collection.get(col).aggregate(aggregate);
				//ReactiveAggregate(this, col, agregate)
				//console.log(collection);
			}else{
				var collection = Meteor.Collection.get(col).find(selector,projection).fetch();
			}
			if(funcMap){
				collection = collection.map(funcMap)
			}
			return collection
		},
		customExcel: function(collection) {
			check(collection,Array)
			var heading = true; // Optional, defaults to true
			var delimiter = "," // Optional, defaults to ",";
			return exportcsv.exportToCSV(collection, heading, delimiter);
		}
	})
}