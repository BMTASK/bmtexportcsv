import { Meteor } from 'meteor/meteor'

findOwnContext = selector => {
	let view = selector instanceof Blaze.View ? selector : selector.constructor === String ? Blaze.getView(document.querySelector(selector)) : Blaze.getView(selector)
	while (!view.hasOwnProperty('templateInstance')) {
		view = view.parentView
	}
	return view.templateInstance()
}

export default downloadTableExcel = (tableID, funcMap, options = {})=>{
	const table = findOwnContext($("#"+tableID)[0])
	const selector = options.selector || table.tabular.selector
	const collection = options.collection || table.tabular.collection.get()._name
	const columns = options.columns || table.tabular.options.get().columns
	const joins = options.joins || table.tabular.options.get().joins
	let project = {}
	project._id=0
	_.each(columns,function(v,k){
		project[v.title]={ $ifNull: [ "$"+v.data, "" ] }
	})
	var nameFile = tableID+'.csv';
	let params ={
		col: collection,
		selector: selector,
		joins: joins,
		aggregate:[
			{
				$project:project
			},
		]
	}
	Meteor.call('downloadPlainExcel',params, function(err, fileContent) {
		if(err){
			console.log(err);
		}else
		if(fileContent){
			if(funcMap){
				fileContent = fileContent.map(funcMap)
			}
			let heading = true; // Optional, defaults to true
			let delimiter = "," // Optional, defaults to ",";
			let result = exportcsv.exportToCSV(fileContent, heading, delimiter);
			let blob = new Blob([result], {type: "text/plain;charset=utf-8"});
			saveAs(blob, nameFile);
		}
	});
}