Package.describe({
  summary: "Export a collection to csv",
  name: "bmt:exportcsv",
  version: "0.1.1",
  git: ""
});

Package.onUse(function(api) {
	api.use([
		'ecmascript',
		'underscore',
	]);
	api.addFiles([
		'lib/exportcsv.js',
		'lib/utils.js'
	], ['client', 'server']);
	api.addFiles([
		'lib/FileSaver.min.js',
	], ['client']);
	api.addFiles([
		'lib/downloadMethod.js'
	], ['server']);
	if (api.export) {
		api.export('exportcsv');
		api.export('saveAs');
		api.export('downloadTableExcel')
	}
});
